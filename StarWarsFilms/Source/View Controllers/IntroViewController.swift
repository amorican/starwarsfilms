//
//  ViewController.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import UIKit
import FrankKit

class IntroViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var activityLabel: UILabel?
    
    lazy var viewModel: FilmsViewModel = {
        let viewModel = FilmsViewModel()
        return viewModel
    }()
    
    deinit {
        cleanupSubscriptions()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControls()
        prepareSubscriptions()
    }
    
    fileprivate func refreshControls() {
        if viewModel.backgroundWorkersCount.value > 0 {
            activityIndicator?.startAnimating()
            activityLabel?.isHidden = false
        }
        else {
            activityIndicator?.stopAnimating()
            activityLabel?.isHidden = true
        }
    }
    
    fileprivate func navigateToMainNavigationView() {
        performSegue(withIdentifier: "introViewToMainNavigationViewSegue", sender: self)
    }
    
    fileprivate func showError() {
        let errorText = viewModel.lastError.value?.localizedDescription ?? "Unknown Error"
        let alert = UIAlertController(title: "Error", message: errorText, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            // Do something here
        }
        let retryAction = UIAlertAction(title: "Try Again", style: .default) { [weak self] (_) in
            self?.viewModel.reloadData()
        }
        alert.addAction(cancelAction)
        alert.addAction(retryAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MainNavigationController {
            destination.viewModel = viewModel
        }
        else { fatalError("Implementation error: Unexpected segue to " + String(describing: segue.destination)) }
    }
}

extension IntroViewController: SubscriptionPreparable {
    func prepareSubscriptions() {
        viewModel.backgroundWorkersCount.unretainedSubscribe(self) { [weak self] (_, _) in
            self?.refreshControls()
        }
        viewModel.state.unretainedSubscribe(self) { [weak self] (_, newValue: FilmsViewModel.State) in
            if newValue == .loaded {
                self?.navigateToMainNavigationView()
            }
            else if newValue == .error {
                self?.showError()
            }
        }
    }
    
    func cleanupSubscriptions() {
        viewModel.backgroundWorkersCount.unretainedUnsubscribe(self)
        viewModel.state.unretainedUnsubscribe(self)
    }
}
