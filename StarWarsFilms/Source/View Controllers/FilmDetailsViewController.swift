//
//  FilmDetailsViewController.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import UIKit

class FilmDetailsViewController: UIViewController {
    
    @IBOutlet weak var filmTitleLabel: UILabel?
    @IBOutlet weak var episodeNumberLabel: UILabel?
    @IBOutlet weak var directorNameLabel: UILabel?
    @IBOutlet weak var producerNameLabel: UILabel?
    @IBOutlet weak var releaseDateLabel: UILabel?
    @IBOutlet weak var backButton: UIButton?
    @IBOutlet weak var crawlView: CrawlTextView?
    
    var film: Film? {
        didSet { refreshControls() }
    }
    
    static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        crawlView?.backgroundColor = .clear
        refreshControls()
    }
    
    fileprivate func refreshControls() {
        guard let film = film else { return }
        filmTitleLabel?.text = film.title
        episodeNumberLabel?.text = "Episode \(film.episodeId)"
        directorNameLabel?.text = film.director
        producerNameLabel?.text = film.producer
        
        if let date = film.releaseDate {
            releaseDateLabel?.text = Self.dateFormatter.string(from: date)
        }
        else { releaseDateLabel?.text = "" }
        
        refreshCrawlView()
    }
    
    fileprivate func refreshCrawlView() {
        var text = film?.openingCrawl ?? ""
        for _ in 0...45 {
            text = "\n" + text
        }
        crawlView?.text = text
        crawlView?.startCrawlingAnimation()
    }
    
    // MARK: - Actions
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
