//
//  FilmsTableViewController.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import UIKit

class FilmsTableViewController: UITableViewController {

    var viewModel: FilmsViewModel? {
        didSet { tableView.reloadData() }
    }
    
    var films: [Film] { return viewModel?.films.value ?? [] }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backgroundColor = .clear
        tableView.backgroundColor = .clear
        let backgroundView = UIImageView(image: UIImage(named: "StarsBackground"))
        backgroundView.frame = view.bounds
        tableView.backgroundView = backgroundView
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return films.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        static let cellIdentifier = "filmCellViewIdentifier"

        let cell = tableView.dequeueReusableCell(withIdentifier: "filmCellViewIdentifier", for: indexPath)
        
        if indexPath.row >= films.count {
            fatalError("Requesting cell for row \(indexPath.row), but data array has \(films.count) rows")
        }
        let film = films[indexPath.row]
        guard let filmCell = cell as? FilmTableViewCell else { return cell }
        filmCell.filmTitleLabel?.text = film.title
        return filmCell
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FilmDetailsViewController,
           let selectedRow = tableView.indexPathForSelectedRow?.row {
            let film = films[selectedRow]
            destination.film = film
        }
        else { fatalError("Implementation error: Unexpected segue to " + String(describing: segue.destination)) }
    }
}
