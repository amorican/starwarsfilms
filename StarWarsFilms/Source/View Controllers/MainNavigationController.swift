//
//  MainNavigationController.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    var viewModel: FilmsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        isModalInPresentation = true
        let topController = topViewController as? FilmsTableViewContainerController
        topController?.viewModel = viewModel
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FilmsTableViewContainerController {
            destination.viewModel = viewModel
        }
        else { fatalError("Implementation error: Unexpected segue to " + String(describing: segue.destination)) }
    }
}
