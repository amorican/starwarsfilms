//
//  FilmTableViewCell.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import UIKit

class FilmTableViewCell: UITableViewCell {

    @IBOutlet weak var filmTitleLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
