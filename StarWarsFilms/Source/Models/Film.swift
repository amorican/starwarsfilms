//
//  Film.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import Foundation
import FrankKit

public struct Film: Modelable, Hashable {

    public let episodeId: Int
    public let title: String
    public let openingCrawl: String
    public let director: String
    public let producer: String
    fileprivate let releaseDateString: String
    
    public init(with representation: [String: Any]) throws {
        self.episodeId = try representation.decode(Schema.episodeId.rawValue)
        self.title = try representation.decode(Schema.title.rawValue)
        self.openingCrawl = try representation.decode(Schema.openingCrawl.rawValue)
        self.director = try representation.decode(Schema.director.rawValue)
        self.producer = try representation.decode(Schema.producer.rawValue)
        self.releaseDateString = try representation.decode(Schema.releaseDate.rawValue)
    }
    
}
extension Film {
    
    public static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD"
        return dateFormatter
    }()

    public static var path = "films"

    public var primaryId: String {
        return "\(episodeId)"
    }
    public var stringValue: String {
        return title
    }
    public var releaseDate: Date? {
        return Self.dateFormatter.date(from: releaseDateString)
    }
    
    
    public struct Schema {
        public static let episodeId = ModelProperty("episode_id", type: Int.self)
        public static let title = ModelProperty("title", type: String.self)
        public static let openingCrawl = ModelProperty("opening_crawl", type: String.self)
        public static let director = ModelProperty("director", type: String.self)
        public static let producer = ModelProperty("producer", type: String.self)
        public static let releaseDate = ModelProperty("release_date", type: String.self)
    }
    public static var properties: [String: ModelProperty] = [Schema.episodeId.rawValue: Schema.episodeId,
                                                             Schema.title.rawValue: Schema.title,
                                                             Schema.openingCrawl.rawValue: Schema.openingCrawl,
                                                             Schema.director.rawValue: Schema.director,
                                                             Schema.producer.rawValue: Schema.producer,
                                                             Schema.releaseDate.rawValue: Schema.releaseDate
    ]
}

// MARK: Equatable
extension Film: Equatable {
    public static func == (lhs: Film, rhs: Film) -> Bool {
        return lhs.primaryId == rhs.primaryId
    }
}

// MARK: CustomStringConvertible
extension Film: CustomStringConvertible {
    public var description: String {
        return title
    }
    public var debugDescription: String {
        return "\(type(of: self)):{\n" +
            "episodeId: \(String(describing: episodeId)),\n\t" +
            "title: \(String(describing: title))\n" +
            "director: \(String(describing: director))\n" +
            "producer: \(String(describing: producer))\n" +
            "releaseDate: \(String(describing: releaseDate))\n" +
        "}"
    }
}

