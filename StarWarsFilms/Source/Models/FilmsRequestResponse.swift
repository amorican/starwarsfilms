//
//  FilmsRequestResponse.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import Foundation
import FrankKit

public struct FilmsRequestResponse: Modelable, Hashable {

    public let count: Int
//    public let next: String
//    public let previous: String
    public let films: [Film]
    
    public init(with representation: [String: Any]) throws {
        self.count = try representation.decode(Schema.count.rawValue)
//        self.next = try representation.decode(Schema.next.rawValue)
//        self.previous = try representation.decode(Schema.previous.rawValue)
        self.films = try representation.decodeArray(Schema.films.rawValue)
    }
    
}
extension FilmsRequestResponse {
    public static var path = "films"

    public var primaryId: String {
        return "Not Applicable"
    }
    public var stringValue: String {
        return "Film Request Response"
    }
    
    public struct Schema {
        public static let count = ModelProperty("count", type: Int.self)
        public static let next = ModelProperty("next", type: String.self)
        public static let previous = ModelProperty("previous", type: String.self)
        public static let films = ModelProperty("results", type: [Film].self)
    }
    public static var properties: [String: ModelProperty] = [Schema.count.rawValue: Schema.count,
                                                             Schema.next.rawValue: Schema.next,
                                                             Schema.previous.rawValue: Schema.previous,
                                                             Schema.films.rawValue: Schema.films
    ]
}

// MARK: Equatable
extension FilmsRequestResponse: Equatable {
    public static func == (lhs: FilmsRequestResponse, rhs: FilmsRequestResponse) -> Bool {
        return false
    }
}

// MARK: CustomStringConvertible
extension FilmsRequestResponse: CustomStringConvertible {
    public var description: String {
        return stringValue
    }
    public var debugDescription: String {
        return "\(type(of: self)):{\n" +
            "count: \(String(describing: count)),\n\t" +
//            "next: \(String(describing: next))\n" +
//            "previous: \(String(describing: previous))\n" +
        "}"
    }
}

