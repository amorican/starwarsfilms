//
//  FilmsViewModel.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import Foundation
import FrankKit

public class FilmsViewModel {
    
    enum State {
        case ready
        case loading
        case loaded
        case error
    }
    
    let state = Subscribable(State.ready)
    let backgroundWorkersCount = Subscribable(0)
    let films = Subscribable([Film]())
    let lastError = Subscribable(nil as Error?)
    
    init() {
        reloadData()
    }
    
    func reloadData() {
        backgroundWorkersCount.value += 1
        state.value = .loading
        beginAsync { [weak self] in
            do {
                let films = try Self.readFilms().asyncWait()
                self?.films.value = films
                self?.state.value = .loaded
            }
            catch {
                self?.lastError.value = error
                self?.state.value = .error
            }
            self?.backgroundWorkersCount.value -= 1
        }
    }
    
    fileprivate static func readFilms() -> Promise<[Film]> {
        return Promise { (completion: @escaping (Promise<[Film]>.ResultType) -> Void) in
            beginAsync {
                do {
                    let results = try SwapiEndpoint().run(path: Film.path).asyncWait()
                    guard let value = results.objectValue else {
                        throw RestEndpointError.invalidResponse("Response value is in an unexpected format.")
                    }
                    let response = try FilmsRequestResponse(with: value)
                    completion(.success(response.films))
                }
                catch { completion(.failure(error)) }
            }
        }
    }
}
