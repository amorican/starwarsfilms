//
//  SwapiAPI.swift
//  StarWarsFilms
//
//  Created by Frank Le Grand on 2/1/21.
//

import Foundation
import FrankKit

public struct SwapiEndpoint: RestEndpoint {
    
    public var rootURL: String { return "https://swapi.dev/api" }
    public var headers: [String : String] { return [:] }
}
